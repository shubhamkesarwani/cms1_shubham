package com.cognizant.CMS.bean;

import java.util.List;

public class Role {

	private String roleName;
	private String Description;
	private List<Module> modules;

	
	public Role(String roleName, String description, List<Module> modules) {
		super();
		this.roleName = roleName;
		Description = description;
		this.modules = modules;
	}

	public List<Module> getModules() {
		return modules;
	}

	public void setModules(List<Module> modules) {
		this.modules = modules;
	}

	public Role() {
		super();
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public String getDescription() {
		return Description;
	}

	public void setDescription(String description) {
		Description = description;
	}

	@Override
	public String toString() {
		return "\n Role [getModules()=" + getModules() + ", getRoleName()=" + getRoleName() + ", getDescription()="
				+ getDescription() + "]";
	}


}
